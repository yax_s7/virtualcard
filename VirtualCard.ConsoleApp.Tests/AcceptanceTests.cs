using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using VirtualCard.ConsoleApp;
using VirtualCard.ConsoleApp.Models;
using VirtualCard.ConsoleApp.Services;

namespace VirtualCard.ConsoleApp.Tests
{
    public class AcceptanceTests
    {
        private BankService _bankService;

        [SetUp]
        public void Initialise()
        {
            _bankService = new BankService(new CardCheckService(), new AccountService());
        }

        [Test]
        public void Given_Valid_Pin_And_Sufficient_Balance_And_Withdraw_Amount__When_Withdraw__Then_Money_Should_Be_Withdrawn()
        {
            var withdrawAmount = 50.50m;
            var card = CreateCard();
            var enteredPin = card.Pin;
            _bankService.Register(card, enteredPin, 100m);
            var initialBalance = _bankService.GetBalance(card, enteredPin);
            
            var success = _bankService.Withdraw(card, enteredPin, withdrawAmount);

            Assert.That(success, Is.True);
            var actualBalance = _bankService.GetBalance(card, enteredPin);
            Assert.That(actualBalance, Is.EqualTo(initialBalance - withdrawAmount));
        }

        [Test]
        public void Given_Deposit_Amount__When_Top_Up__Then_Money_Should_Be_Deposited()
        {
            var card = CreateCard();
            var enteredPin = card.Pin;
            _bankService.Register(card, enteredPin, 20m);
            var initialBalance = _bankService.GetBalance(card, enteredPin);
            var depositAmount = 10m;
            
            var success = _bankService.Deposit(card, enteredPin, depositAmount);

            Assert.That(success, Is.True);
            var actualBalance = _bankService.GetBalance(card, enteredPin);
            Assert.That(actualBalance, Is.EqualTo(initialBalance + depositAmount));
        }    
        
        [Test]
        [Repeat(1000)]
        public async Task Given_Concurrent_Withdrawals_And_Deposits_And_In_Sufficient_Balance_When_Withdraw_Then_Money_Should_Not_Be_Withdrawn()
        {
            var pin = "1234";
            var card1 = CreateCard(pin: pin);
            var card2 = CreateCard(pin: pin);
            var initialBalance = 0;
            _bankService.Register(card1, pin, initialBalance);

            var t1 = Task.Factory.StartNew(() => _bankService.Deposit(card1, pin, 20m));
            var t2 = Task.Factory.StartNew(() => _bankService.Withdraw(card1, pin, 20m));
            var t3 = Task.Factory.StartNew(() => _bankService.Deposit(card1, pin, 10m));
            var t4 = Task.Factory.StartNew(() => _bankService.Withdraw(card1, pin, 10m));

            await Task.WhenAll(new [] {t1, t2, t3, t4 });

            var balance = _bankService.GetBalance(card1, pin);
            Assert.That(balance, Is.EqualTo(0m));
        }
        
        private Card CreateCard(string accountNumber = "220022", string pin = "1111")
        {
            var card = new Card(accountNumber, pin);
            return card;
        }
    }
}