using NUnit.Framework;
using VirtualCard.ConsoleApp.Models;
using VirtualCard.ConsoleApp.Services;

namespace VirtualCard.ConsoleApp.Tests
{
    public class CardCheckServiceTests
    {
        private CardCheckService _cardCheckService;

        [SetUp]
        public void Initialise()
        {
            _cardCheckService = new CardCheckService();
        }

        [Test]
        public void Given_Incorrect_Pin__When_IsPinValid__Then_Returns_False()
        {
            var card = new Card("2202200", "1234");
            var incorrectPin = "1111";

            var isValid = _cardCheckService.IsPinValid(card, incorrectPin);

            Assert.That(isValid, Is.False);
        }

        [Test]
        public void Given_Valid_Pin__When_IsPinValid__Then_Returns_True()
        {
            var configuredPin = "1234";
            var card = new Card("2202200", configuredPin);

            var isValid = _cardCheckService.IsPinValid(card, configuredPin);

            Assert.That(isValid, Is.True);
        }
    }
}