using System;
using NUnit.Framework;
using VirtualCard.ConsoleApp.Models;
using VirtualCard.ConsoleApp.Services;
using Moq;
using VirtualCard.ConsoleApp.Exceptions;
using VirtualCard.ConsoleApp;

namespace VirtualCard.ConsoleApp.Tests
{
    public class BankServiceTests
    {
        private Mock<ICardCheckService> _mockCardCheckService;
        private Mock<IAccountService> _mockAccountService;
        private BankService _sut;

        [SetUp]
        public void Initialise()
        {
            _mockCardCheckService = new Mock<ICardCheckService>();
            _mockAccountService = new Mock<IAccountService>();
            _sut = new BankService(_mockCardCheckService.Object, _mockAccountService.Object);
        }

        [Test]
        public void Given_Card__When_Register__Then_Should_Check_Valid_Pin()
        {
            var card = new Card("2220222", "1234");
            
            _sut.Register(card, "1234");

            _mockCardCheckService.Verify(x => x.IsPinValid(It.IsAny<Card>(), It.IsAny<string>()));
        }

        [Test]
        public void Given_Valid_Pin_And_Unregistered_Card__When_Get_Balance__Then_Should_Throw_Bank_Exception()
        {
            var card = SetupWithNoAccount(true);

            var ex = Assert.Throws<BankException>(() => _sut.GetBalance(card, "222"));

            Assert.That(ex.Message, Is.EqualTo(ErrorMessages.AccountNotRegistered));
        }

        [Test]
        public void Given_Valid_Pin_And_Unregistered_Card__When_Deposit__Then_Should_Throw_Bank_Exception()
        {
            var card = SetupWithNoAccount(true);

            var ex = Assert.Throws<BankException>(() => _sut.Deposit(card, "222", 10m));

            Assert.That(ex.Message, Is.EqualTo(ErrorMessages.AccountNotRegistered));
        }

        [Test]
        public void Given_Valid_Pin_And_Unregistered_Card__When_Withdraw__Then_Should_Throw_Bank_Exception()
        {
            var card = SetupWithNoAccount(true);

            var ex = Assert.Throws<BankException>(() => _sut.Withdraw(card, "222", 10m));

            Assert.That(ex.Message, Is.EqualTo(ErrorMessages.AccountNotRegistered));
        }

        [Test]
        public void Given_Valid_Pin_And_Registered_Card_And_Sufficient_Balance__When_Get_Balance__Then_Should_Call_Get_Account()
        {
            var enteredPin = "2222";
            var card = SetupWithAccount(true, 10);

            var balance = _sut.GetBalance(card, enteredPin);

            _mockAccountService.Verify(x => x.GetAccount(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void Given_Valid_Pin_And_Registered_Card_When_Deposit_Positive_Amount__Then_Should_Call_Get_Account()
        {
            var enteredPin = "2222";
            var card = SetupWithAccount(true, 10);

            _sut.Deposit(card, enteredPin, 10m);

            _mockAccountService.Verify(x => x.GetAccount(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void Given_Valid_Pin_And_Registered_Card_When_Deposit_Negative_Amount__Then_Should_Not_Call_Get_Account()
        {
            var enteredPin = "2222";
            var card = SetupWithAccount(true, 10);

            _sut.Deposit(card, enteredPin, -10m);

            _mockAccountService.Verify(x => x.GetAccount(It.IsAny<string>()), Times.Never);
        }

        [Test]
        public void Given_Valid_Pin_And_Registered_Card__When_Withdraw_Positive_Amount__Then_Should_Call_Get_Account()
        {
            var enteredPin = "2222";
            var card = SetupWithAccount(true, 10);

            _sut.Withdraw(card, enteredPin, 5m);

            _mockAccountService.Verify(x => x.GetAccount(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void Given_Valid_Pin_And_Registered_Card__When_Withdraw_Negative_Amount__Then_Should_Not_Call_Get_Account_And_Not_Subtract_Balance()
        {
            var enteredPin = "2222";
            var card = SetupWithAccount(true, 10);

            _sut.Withdraw(card, enteredPin, -10m);

            _mockAccountService.Verify(x => x.GetAccount(It.IsAny<string>()), Times.Never);
        }

        [Test]
        public void Given_Card_With_Invalid_Pin__When_Get_Balance__Then_Should_Throw_Invalid_Pin_Exception()
        {
            var enteredPin = "2223";
            var card = SetupWithAccount(false, 10);

            var ex = Assert.Throws<BankException>(() => _sut.GetBalance(card, enteredPin));

            Assert.That(ex.Message, Is.EqualTo(ErrorMessages.InvalidPin));
        }

        [Test]
        public void Given_Card_With_Invalid_Pin__When_Deposit__Then_Should_Throw_Invalid_Pin_Exception()
        {
            var enteredPin = "2223";
            var card = SetupWithAccount(false, 10);

            var ex = Assert.Throws<BankException>(() => _sut.Deposit(card, enteredPin, 4m));

            Assert.That(ex.Message, Is.EqualTo(ErrorMessages.InvalidPin));
        }

         [Test]
        public void Given_Card_With_Invalid_Pin__When_Withdraw__Then_Should_Throw_Invalid_Pin_Exception()
        {
            var enteredPin = "2223";
            var card = SetupWithAccount(false, 10);

            var ex = Assert.Throws<BankException>(() => _sut.Withdraw(card, enteredPin, 4m));

            Assert.That(ex.Message, Is.EqualTo(ErrorMessages.InvalidPin));
        }
        
        private Card SetupWithAccount(bool isPinValid, decimal balance)
        {
            var card = new Card("111", "2222");
            _mockCardCheckService.Setup(x => x.IsPinValid(card, It.IsAny<string>()))
                .Returns(isPinValid);
            _mockAccountService.Setup(x => x.GetAccount(card.AccountNumber))
                .Returns(new Account(card.AccountNumber, balance));
            return card;
        }

        private Card SetupWithNoAccount(bool isPinValid)
        {
            var card = new Card("111", "2222");
            _mockCardCheckService.Setup(x => x.IsPinValid(card, It.IsAny<string>()))
                .Returns(isPinValid);
            _mockAccountService.Setup(x => x.GetAccount(card.AccountNumber))
                .Returns(default(Account));
            return card;
        }
    }
}