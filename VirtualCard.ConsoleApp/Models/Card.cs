using System;

namespace VirtualCard.ConsoleApp.Models
{
    public class Card
    {
        public string Pin { get; }
        public string AccountNumber { get; }

        public Card(string accountNumber, string pin)
        {
            // TODO validation?
            Pin = pin;
            AccountNumber = accountNumber;
        }
    }
}