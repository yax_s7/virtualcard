using System;

namespace VirtualCard.ConsoleApp.Exceptions
{
    public class BankException : Exception
    {
        public BankException(string message) : base(message)
        {
            
        }
    }
}