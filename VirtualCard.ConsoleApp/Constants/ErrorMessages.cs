namespace VirtualCard.ConsoleApp
{
    public class ErrorMessages
    {
        public const string PinNotSetup = "Pin not setup";

        public const string InvalidPin = "Invalid pin";
        public static string AccountNotRegistered = "Account not found";
    }
}