using VirtualCard.ConsoleApp.Models;

namespace VirtualCard.ConsoleApp.Services
{
    public interface ICardCheckService
    {
        bool IsPinValid(Card card, string enteredPin);
    }
}