using System;
using System.Collections.Generic;
using VirtualCard.ConsoleApp.Exceptions;
using VirtualCard.ConsoleApp.Models;

namespace VirtualCard.ConsoleApp.Services
{
    public class BankService
    {
        private static readonly object locker = new Object();
        public readonly Dictionary<string, decimal> _cardBalances;
        private readonly ICardCheckService _cardCheckService;
        private readonly IAccountService _accountService;

        public BankService(ICardCheckService cardCheckService, IAccountService accountService)
        {
            _cardBalances = new Dictionary<string, decimal>();
            _cardCheckService = cardCheckService;
            _accountService = accountService;
        }

        public void Register(Card card, string enteredPin, decimal initialBalance = 0m)
        {
            if (!_cardCheckService.IsPinValid(card, enteredPin))
            {   
                return;
            }

            lock (locker)
            {
                _accountService.Register(card.AccountNumber, initialBalance);
            }
        }

        public decimal GetBalance(Card card, string enteredPin)
        {
            lock (locker)
            {
                var account = GetAccount(card, enteredPin);

                return account.Balance;
            }
        }

        
        public bool Withdraw(Card card, string enteredPin, decimal amount)
        {
            if (amount < 0)
            {
                return false;
            }

            lock (locker)
            {
                var account = GetAccount(card, enteredPin);
                _accountService.SetAmount(account.AccountNumber, amount * -1);
                return true;
            }
        }

        public bool Deposit(Card card, string enteredPin, decimal amount)
        {
            if (amount < 0)
            {
                return false;
            }

            lock (locker)
            {
                var account = GetAccount(card, enteredPin);
                _accountService.SetAmount(account.AccountNumber, amount);
                return true;
            }
        }

        private Account GetAccount(Card card, string enteredPin)
        {
            if (!_cardCheckService.IsPinValid(card, enteredPin))
            {
                throw new BankException(ErrorMessages.InvalidPin);
            }

            var account = _accountService.GetAccount(card.AccountNumber);
            if (account == null)
            {
                throw new BankException(ErrorMessages.AccountNotRegistered);
            }

            return account;
        }
    }
}