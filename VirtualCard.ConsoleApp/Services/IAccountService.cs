using VirtualCard.ConsoleApp.Models;

namespace VirtualCard.ConsoleApp.Services
{
    public interface IAccountService
    {
        Account GetAccount(string accountNumber);
        void Register(string accountNumber, decimal initialBalance = 0m);
        void SetAmount(string accountNumber, decimal amount);
    }
}