using System.Collections.Generic;
using VirtualCard.ConsoleApp.Models;

namespace VirtualCard.ConsoleApp.Services
{
    public class AccountService : IAccountService
    {
        public readonly Dictionary<string, Account> _accounts;

        public AccountService()
        {
            _accounts = new Dictionary<string, Account>();
        }

        public Account GetAccount(string accountNumber)
        {
            if (!_accounts.ContainsKey(accountNumber))
            {
                return null;
            }

            return _accounts[accountNumber];
        }

        public void Register(string accountNumber, decimal initialBalance = 0m)
        {
            if (!_accounts.ContainsKey(accountNumber))
            {
                _accounts.Add(accountNumber, new Account(accountNumber, initialBalance));
            }
        }

        public void SetAmount(string accountNumber, decimal amount)
        {
            if (_accounts.ContainsKey(accountNumber))
            {
                System.Console.WriteLine($"Balance: {_accounts[accountNumber].Balance} Amount: {amount}");
                _accounts[accountNumber].Balance += amount;
            }
        }
    }
}