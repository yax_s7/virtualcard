using VirtualCard.ConsoleApp.Models;

namespace VirtualCard.ConsoleApp.Services
{
    public class CardCheckService : ICardCheckService
    {
        public bool IsPinValid(Card card, string enteredPin)
        {
            return card.Pin == enteredPin;
        }
    }
}